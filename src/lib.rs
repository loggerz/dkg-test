use crate::kyber_lib::compute;
use wasm_bindgen::prelude::*;

// pub mod dkg_lib;
pub mod kyber_lib;

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen]
pub fn test_from_kyber() -> String {
    compute()
}