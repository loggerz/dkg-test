use std::collections::HashMap;
use std::ops::Deref;
use dkg::{Participant, frost::KeyGenMachine, ThresholdCore, ThresholdParams, lagrange};
use ciphersuite::{Ciphersuite, Ed25519};
use ciphersuite::group::ff::Field;
use ciphersuite::group::GroupEncoding;
use dalek_ff_group::{EdwardsPoint, Scalar};
use dkg::encryption::{EncryptedMessage, EncryptionKeyMessage};
use dkg::frost::{KeyMachine, SecretShare};
use rand::rngs::StdRng;
use rand::{CryptoRng, RngCore, SeedableRng};

#[cfg(target_arch = "wasm32")]
use wasm_bindgen::prelude::*;
#[cfg(target_arch = "wasm32")]
use web_sys::{console};

type FrostEncryptedMessage<C> = EncryptedMessage<C, SecretShare<<C as Ciphersuite>::F>>;
type FrostSecretShares<C> = HashMap<Participant, FrostEncryptedMessage<C>>;

const CONTEXT: &str = "DKG Test Key Generation";
const PARTICIPANTS: u16 = 4; // 5;
const THRESHOLD: u16 = 3; // ((PARTICIPANTS / 3) * 2) + 1;

pub fn clone_without<K: Clone + std::cmp::Eq + std::hash::Hash, V: Clone>(
    map: &HashMap<K, V>,
    without: &K,
) -> HashMap<K, V> {
    let mut res = map.clone();
    res.remove(without).unwrap();
    res
}

// Commit, then return enc key and shares
#[allow(clippy::type_complexity)]
fn commit_enc_keys_and_shares<R: RngCore + CryptoRng, C: Ciphersuite>(
    rng: &mut R,
) -> (
    HashMap<Participant, KeyMachine<C>>,
    HashMap<Participant, FrostSecretShares<C>>,
) {
    let mut machines = HashMap::new();
    let mut commitments = HashMap::new();
    for i in (1 ..= PARTICIPANTS).map(|i| Participant::new(i).unwrap()) {
        let params = ThresholdParams::new(THRESHOLD, PARTICIPANTS, i).unwrap();
        let machine = KeyGenMachine::<C>::new(params, CONTEXT.to_string());
        let (machine, these_commitments) = machine.generate_coefficients(rng);
        machines.insert(i, machine);

        commitments.insert(
            i,
            EncryptionKeyMessage::read::<&[u8]>(&mut these_commitments.serialize().as_ref(), params)
                .unwrap(),
        );
    }

    let mut secret_shares = HashMap::new();
    let machines = machines
        .drain()
        .map(|(l, machine)| {
            let (machine, mut shares) =
                machine.generate_secret_shares(rng, clone_without(&commitments, &l)).unwrap();
            let shares = shares
                .drain()
                .map(|(l, share)| {
                    (
                        l,
                        EncryptedMessage::read::<&[u8]>(
                            &mut share.serialize().as_ref(),
                            // Only t/n actually matters, so hardcode i to 1 here
                            ThresholdParams::new(THRESHOLD, PARTICIPANTS, l).unwrap(),
                        ).unwrap(),
                    )
                })
                .collect::<HashMap<_, _>>();
            secret_shares.insert(l, shares);
            (l, machine)
        })
        .collect::<HashMap<_, _>>();

    (machines, secret_shares)
}

fn generate_secret_shares<C: Ciphersuite>(
    shares: &HashMap<Participant, FrostSecretShares<C>>,
    recipient: Participant,
) -> FrostSecretShares<C> {
    let mut our_secret_shares = HashMap::new();
    for (i, shares) in shares {
        if recipient == *i {
            continue;
        }
        our_secret_shares.insert(*i, shares[&recipient].clone());
    }
    our_secret_shares
}

pub fn frost_gen<R: RngCore + CryptoRng, C: Ciphersuite>(
    rng: &mut R,
) -> HashMap<Participant, ThresholdCore<C>> {
    let (mut machines, secret_shares) = commit_enc_keys_and_shares::<_, C>(rng);

    let mut group_key = None;
    machines
        .drain()
        .map(|(i, machine)| {
            let our_secret_shares = generate_secret_shares(&secret_shares, i);
            let these_keys = machine.calculate_share(rng, our_secret_shares).unwrap().complete();

            // Verify the group keys are agreed upon
            if group_key.is_none() {
                group_key = Some(these_keys.group_key());
            }
            assert_eq!(group_key.unwrap(), these_keys.group_key());

            (i, these_keys)
        })
        .collect::<HashMap<_, _>>()
}

pub fn test_from_dkg_lib() -> String {
    let mut rng = StdRng::from_entropy();

    let keys = frost_gen::<StdRng, Ed25519>(&mut rng);

    let p1 = Participant::new(1).unwrap();
    let threshold_core1 = &keys[&p1];

    let group_pubkey: EdwardsPoint = threshold_core1.group_key();
    let group_pubkey_bytes = &group_pubkey.to_bytes();

    // Group key generation
    let included = keys.keys().cloned().collect::<Vec<_>>();
    let group_key = keys.iter().fold(Scalar::zero(), |accum, (i, keys)| {
        accum + (lagrange::<Scalar>(*i, &included) * keys.secret_share().deref())
    });
    let group_key_bytes = &group_key.to_bytes();

    #[cfg(target_arch = "wasm32")]
    {
        console::log_1(&format!("group pubkey : {:?}", &group_pubkey_bytes).into());
        console::log_1(&format!("group key : {:?}", &group_key_bytes).into());
    }
    println!("group pubkey : {:?}", &group_pubkey_bytes);
    println!("group key : {:?}", &group_key_bytes);

    hex::encode(group_pubkey_bytes)
}