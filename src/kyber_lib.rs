use kyber_rs::{
//    encrypt::{encrypt, decrypt},
    group::edwards25519::SuiteEd25519,
    util::key::new_key_pair
};
use kyber_rs::group::edwards25519::{Point, Scalar};
use kyber_rs::share::dkg::pedersen::structs::{Deal, Response};
use kyber_rs::share::dkg::pedersen::{DistKeyGenerator, new_dist_key_generator};
use kyber_rs::util::key::Pair;
use std::borrow::{BorrowMut};
use std::collections::HashMap;
use kyber_rs::share::poly::PriShare;
use kyber_rs::encrypt::{encrypt, decrypt};
use kyber_rs::share::poly::recover_secret;

const PARTICIPANTS: usize = 4;
const THRESHOLD: usize = 3;

#[derive(Clone)]
struct FakeReader {}
impl std::io::Read for FakeReader {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        Err(std::io::ErrorKind::Unsupported.into())
    }
}

pub struct ComputeSide {
    suite: SuiteEd25519,
    keys: Pair<Point>,
    dkg: DistKeyGenerator<SuiteEd25519, FakeReader>,
    /// deals received
    deals: Vec<Deal<Point>>,
    /// responses received
    responses: Vec<Response>,
}

impl ComputeSide {
    fn new(suite: SuiteEd25519, keys: Pair<Point>, pubkeys: &Vec<Point>) -> Self {
        Self {
            suite,
            keys: keys.clone(),
            dkg: new_dist_key_generator(
                suite,
                keys.private,
                &pubkeys,
                THRESHOLD
            ).unwrap(),
            deals: vec![],
            responses: vec![],
        }
    }
}

pub fn compute() -> String {
    let suite = SuiteEd25519::new_blake3_sha256_ed25519();

    let keys: Vec<Pair<Point>> = (0..PARTICIPANTS).map(|_| new_key_pair(&suite).unwrap()).collect();
    let pubkeys: Vec<Point> = keys.iter().map(|pair| pair.public.clone()).collect();

    let mut compute_sides: Vec<ComputeSide> = keys.iter()
        .map(|keys| ComputeSide::new(suite.clone(), keys.clone(), &pubkeys))
        .collect();

    // 3. Each node sends its Deals to the other nodes
    for i in 0..PARTICIPANTS {
        let deals = compute_sides[i].borrow_mut().dkg.deals().unwrap();
        for (i, deal) in deals {
            compute_sides[i].deals.push(deal.clone());
        }
    }

    // 4. Process the deals on each node and send the responses to the others
    for i in 0..PARTICIPANTS {
        let deals = compute_sides[i].deals.clone();
        for deal in deals {
            let response = compute_sides[i].borrow_mut().dkg.process_deal(&deal).unwrap();
            for (j, otherSide) in compute_sides.iter_mut().enumerate() {
                if j != i {
                    otherSide.responses.push(response.clone());
                }
            }
        }
    }

    // 5. Process the responses on each node
    for (i, side) in &mut compute_sides.iter_mut().enumerate() {
        println!("Processing responses of node {}", i);
        for response in &side.responses {
            println!("Processing response index {}", response.index);
            if let Some(j) = side.dkg.process_response(response).unwrap() {
                println!("Got a justification, index: {}", j.index);
            }
        }
    }

    // 6. Check and print the qualified shares
    for (i, side) in compute_sides.iter().enumerate() {
        println!("Node {} is certified ? {}", i, side.dkg.certified());
    }

    let mut shares: HashMap<usize, PriShare<Scalar>> = HashMap::new();
    let mut group_pubkey: Option<Point> = None;
    for (i, side) in compute_sides.iter().enumerate() {
        println!("Computing dist key share of node {}", i);
        let dist_key_share = side.dkg.dist_key_share().unwrap();
        shares.insert(i, dist_key_share.share);
        let pubkey = dist_key_share.public();
        match group_pubkey {
            Some(p) => if p != pubkey {
                panic!("Different group public keys");
            },
            None => group_pubkey = Some(pubkey)
        };
    }
    let group_pubkey = group_pubkey.unwrap();

    println!("Group Pubkey: {}", group_pubkey.to_string());

    let message: String = "This is my extremley intertesting message ;)".to_string();
    println!("Encrypting message '{}'", message);
    let encrypted_bytes = encrypt(suite.clone(), group_pubkey, &message.as_bytes()).unwrap();

    let shares: Vec<Option<PriShare<Scalar>>> = shares.values().into_iter().map(|v| Some(v.clone())).collect();
    let group_key = recover_secret(suite.clone(), &shares, PARTICIPANTS, THRESHOLD).unwrap();
    println!("Group Key: {}", group_key.to_string());
    let decrypted_bytes = decrypt(suite.clone(), group_key, &encrypted_bytes).unwrap();

    let decrypted_message = String::from_utf8(decrypted_bytes).unwrap();
    println!("Decrypted message : '{}'", decrypted_message);

    println!("happy ;)");
    group_pubkey.to_string()
}